import axios from 'axios';

function LandingPage() {

  async function test (test) {
    await axios({
      method: 'post',
      url: 'http://localhost:3000/test',
      data: {
        firstName: 'Fred',
        lastName: 'Flintstone'
      }
    }).then(res => {
      console.log(res)
    });
  };
  

  return (
    <div className="App">
      <button onClick={e => {test()}}></button>
    </div>
  );
}

export default LandingPage;